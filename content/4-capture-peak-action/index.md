# 4. Capture Peak Action

![](images/1.jpg)

*Level: Introduction*
*Time: 3-5 Minutes*

### The best photos appear to come alive because they contain peak action. This lesson will teach you to capture photos with peak action.

---

A photograph must illustrate a story or scene in a single moment. To do that it has to be the right moment. This is called the moment of peak action.

---

![](images/2.jpg)

#### The pitcher is captured in mid-throw, the moment of peak action.

---

Good photographs are about action and emotion. The most powerful images capture the moment at a scene when action and emotions are at their most visually expressive.

---

![](images/1.jpg)

#### Peak action is shown here at the moment the batter strikes the ball.

---

Look for the moment of peak action when a behavior completes. This moment is often the height of peak action.

---

![](images/3.jpg)

#### Peak action is shown here at the moment the player reacts.

---

The moment of peak action can be a powerful reaction. Don't focus only on primary characters, look to bystanders or the audience for great visual moments.

---

![](images/4.jpg)

#### Peak action is shown here at the moment the players interact.

---
Don't photograph the first moment. Observe the scene, look for action, then wait for the right time to capture your moment.

---

![](images/5.jpg)

#### Peak action is here in the the player's frustration.

---

It is important to remember that sometimes a moment of peak action is more subtle, even intimate.

Anticipation and position are key to capturing peak action. Do not chase the action. Set up and wait to capture the best moment to tell your story.

---

## Examples

---

### Example 1

![](images/example_1.jpg)

---

### Example 2

![](images/example_2.jpg)

---

### Example 3

![](images/example_3.jpg)

---
