# 3. Create Layers

![](images/1.jpg)

*Level: Introduction*
*Time: 3-5 Minutes*

### Layers help the two dimensional space of a photograph better represent the three-dimensional world. This lesson will teach you the basics steps to create photos with layers.

---

Complex photos typically have three layers. The most basic way use layers is with two distinct layers: a subject layer, and a context layer to provide additional information related to your story.

---

## Subject Layer

![](images/2.jpg)

#### In this photo the subject layer is in color.

The subject layer shows the main subject of your photo. This is usually a person or an object. The subject layer is often placed in the foreground layer of your frame.

---

## Context Layer

![](images/3.jpg)

#### Now the context layer is in color.

The context layer is often the background layer. It helps the viewer better understand a subject's environment or an important detail of the story. In this photo the context layer tells the viewer the women are at a protest.

---

## Reversing Layers

![](images/4.jpg)

#### Sometimes the layers are reversed, here the context layer is in the foreground.

The photographer reversed the order of subject and context layers in this shot. The image focuses on the child, but demonstrates the context by placing the chess pieces in the foreground. This technique allows the photographer to maximize the interesting visual information in the frame.

---

## Examples

---

### Example 1

![](images/example_1.jpg)

---

### Example 2

![](images/example_2.jpg)

---

### Example 3

![](images/example_3.jpg)

---
