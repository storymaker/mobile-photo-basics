# 2. Compose with a Grid

![](images/1.jpg)

*Level: Introduction*
*Time: 3-5 Minutes*

### Using a grid to improve composition is the most basic concept a photographer needs to understand. This lesson will teach you to look at your photo as a grid.

---

Think of your photo as if it is made up of nine equal rectangles. Strong visuals use all the rectangles in the grid to tell the story.

![](images/2.jpg)

#### Placing the grid on this photo divides one image into nine equal areas.

---

Most smartphones allow you to turn this grid on in your Camera app. Use the grid when practicing these lessons.

---

![](images/3.jpg)

#### Do not place your subject in the middle with no thought to the rest of the grid.

---

This grid is not intended to help you center your photo. Let's look at how the lines of the grid work together to help you compose better photos.

---

![](images/4.jpg)

#### This line shows the bottom third of the frame.

If the photo is a landscape, this line is often used to establish the horizon in a grid.

---

![](images/5.jpg)

#### This line shows the top third of the frame.

If the photo is a portrait, the subject's eyes are usually placed on the top horizontal line when framing with a grid. As you practice this more you may find that sometimes the bottom line works better.

---

![](images/6.jpg)

#### Note how important elements of the composition are placed in the grid.

To use a grid most effectively, the vertical and horizontal lines should work together to define the frame.

---

![](images/7.jpg)

#### In this photo main subjects complete entirely within the frame.

---

Clearly define the edges of your frame. Try to have important shapes complete within the frame.

---

![](images/8.jpg)

Fill the entire frame with visual information. Use the grid to organize that information and create more compelling photos.

----

## Examples

---

### Example 1

![](images/compose_example_1.jpg)

---

### Example 2

![](images/compose_example_2.jpg)

---

### Example 3

![](images/compose_example_3.jpg)

---
