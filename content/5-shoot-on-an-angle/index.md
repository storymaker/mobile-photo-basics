# 5. Shoot on an Angle

![](images/1.jpg)

*Level: Introduction*
*Time: 3-5 Minutes*

### Too often photographers simply stand in front of their subject and snap a photo. This lesson will demonstrate how to change your angle to make more compelling photos.

---

Choosing a more interesting angle from which to photograph a subject is a simple way to make stronger photos.

---

![](images/2.jpg)

#### Shooting from a low angle can make a subject appear more powerful.

---

Explore a variety of angles when photographing a character or place. A low angle has a very different effect than a high angle.

---

![](images/3.jpg)

#### A slightly elevated angle can help provide information about the scene.

---

An unusual angle can help you emphasize a particular element of your story. Unusual angles can emphasize action and make your photos come alive.

---

![](images/4.jpg)

#### This image emphasizes the chess piece, illustrating the story more dramatically.

---

A very high angle can help better capture an entire scene in a single image.

---

![](images/5.jpg)

#### Unique angles can help you present the story in an interesting and creative way.

---

## Examples

---

### Example 1

![](images/example_1.jpg)

---

### Example 2

![](images/example_2.jpg)

---

### Example 3

![](images/example_3.jpg)

---
