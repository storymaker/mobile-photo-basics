# 6. Control Light

![](images/1.jpg)

*Level: Introduction*
*Time: 5-10 Minutes*

### Control of light and proper exposure is essential to strong mobile photography. Mobile cameras can take beautiful photos but present some unique challenges. This lesson will teach you to control light effectively.

---

Mobile cameras struggle with high contrast light––bright sunlight and deep shadow in the same frame. They also struggle in low light. Learning to solve lighting problems and use light creatively in your photos is key to better photography.

---

![](images/2.jpg)

#### This photo is blurry because mobiles  cannot freeze fast motion  on an overcast day.

---

When possible choose location and time of day for maximum available light. You may also choose a moment to take your photo when your subject is still or moving slowly.

---

![](images/3.jpg)

#### Photos of the same subject can change dramatically depending on the time of day. Early morning and sunset are the most dramatic times to photograph.

---


Mobile cameras have touch-screen exposure. You can change exposure values of your photo by touching a different spot on the screen. NOTE: In your native camera app this will also change your focus.

---

![](images/4.jpg)

#### Experiment with touching different points on the screen to see how the exposure changes.

---

Sometimes changing the exposure point is not enough. Unwanted backlighting is a common problem. Avoid strong light sources behind your subject. Change your angle or move the subject to control strong backlighting.

---

![](images/5.jpg)

#### In the photo on the bottom, the photographer moved to the left to remove the backlight.

---

Once you have control of basic lighting you can begin to use light to create strong photos.

---

Rembrandt Lighting is a technique that uses soft light from the side to improve a portrait. This could be light from a window, for example. This technique is named for a famous painter.

![](images/6.jpg)

#### In the photo on the left, soft light from one side creates more flattering contours on the subject's face.

---

You can do other creative things with lighting. For example, silhouetting takes advantage of strong backlighting.

![](images/7.jpg)

#### In this photo the exposure is set for the much brighter background, creating a silhouette.

---

Some of the more advanced features in your mobile’s camera app can help solve lighting problems. Exposure compensation, for example.

![](images/8.jpg)

#### Explore some of the more advanced features in your camera app's menu.

---

## Examples

---

### Example 1

![](images/example_1.jpg)

---

### Example 2

![](images/example_2.jpg)

---

### Example 3

![](images/example_3.jpg)

---
