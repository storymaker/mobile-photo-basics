# Conoce tu teléfono móvil

![](images/1.jpg)

### Las fotografías son extremadamente poderosas. Esta lección le enseñará los fundamentos del uso de la cámara de su móvil. También se presentarán el resto de las lecciones de este paquete.

----

Una cámara de móvil ve el mundo de forma muy diferente que su ojo. Necesita comprender las limitaciones para conseguir grandes fotos. Es esencial aprencer cómo funciona la cámara de su móvil.

![](images/2.jpg)

***Compruebe el menú de su cámara, especialmente cómo cambiar el foco y la exposición.***

Compruebe el menú de su cámara, especialmente cómo cambiar el foco y la exposición.

Si su cámara tiene una característica de exposición, experimente para ver como cambia la iluminación.

Tu ojo se enfoca rápidamente en la cosa singular de interés en tu campo visual. A una cámara digital se le debe decir dónde enfocarse, especialmente cuando hay poca luz, y puede ser lenta en su enfoque.

---

![](images/3.jpg)

***Toca tu pantalla y la aplicación de tu cámara se enfocará en tal punto.***

Si su sujeto está demasiado oscuro, pulse sobre un punto más luminoso en la pantalla junto al sujeto para encontrar un mejor balance entre exposición y foco.

Muchas de las cámaras de los teléfonos móviles tienen un zoom integrado, pero a menudo exageran el tamaño del pixel y que tan borroso es. Tus imágenes serán más claras y de mejor calidad si en su lugar te colocas más próximo a tu sujeto.

---

#### En la foto de abajo se utilizó el zoom digital.

![](images/5.jpg)

#### En la foto de arriba el fotógrafo se movió para estar más cerca del sujeto.

---

Las aplicaciones avanzadas en la cámara de tu teléfono móvil te ayudarán a hacer más con tu cámara. Explora el menú de aplicaciones de tu cámara.

![](images/6.jpg)

#### Revisa la compensación de exposición. flash y configuraciones HDR en las aplicaciones de tu cámara.

---

Once you understand your mobile camera, you can move on to the remaining five lessons in this pack. Each lesson is presented much like this one and will cover a basic skill.

The lessons are:

* Composing with a Grid
* Creating Layers
* Capturing Peak Action
* Using Angle of View
* Understanding Light

Felicidades, ha aprendido los fundamentos de cómo usar su cámara. Está listo para continuar con las siguientes cinco lecciones. Cada lección se presenta de forma muy parecida a esta, y cubrirá una habilidad básica.

---
