# Know Your Mobile

![](images/1.jpg)

### Photographs are extremely powerful online. This lesson will teach you the basics of using your mobile camera. It will also introduce the rest of the lessons in this pack.

----

A mobile camera is very limited compared to your eye. You need to understand and manage those limitations. It is essential you know how the native camera app in your mobile device works.

![](images/2.jpg)

***At a minimum you need to understand how focus and exposure work in your camera.***

Most native camera apps use a colored ring to set the focus and exposure. Note the small sun icon. It allows you to fine tune exposure.

Your eye is quick to focus on the singular thing of interest in your field of view. A digital camera must be told where to focus, especially in low light, and it can be slow to focus.

---

![](images/3.jpg)

***Touch the screen and your camera app will focus on that point.***


This is also how your camera sets exposure. If your subject is too dark, touch a lighter point on the screen near the subject to find a better balance between exposure and focus.

Most mobile cameras have built-in digital zoom, but it will often exaggerate pixel size and blurriness. Your pictures will be clearer and of better quality if instead you get closer to your subject.

![](images/5.jpg)

#### In the photo on the top digital zoom was used. In the photo on the bottom the photographer moved closer to the subject.

---

The advanced features in your mobile''s camera will help you do more with your camera. Explore the camera app menu.

![](images/6.jpg)

#### Check out the exposure compensation, flash and HDR features in your camera app.

---

Once you understand your mobile camera, you can move on to the remaining five lessons in this pack. Each lesson is presented much like this one and will cover a basic skill.

The lessons are:

* Composing with a Grid
* Creating Layers
* Capturing Peak Action
* Using Angle of View
* Understanding Light

Congratulations, you have learned the basic of how to use your camera. Practice your new skills with the activities below and then work through the rest of the lessons.

---
